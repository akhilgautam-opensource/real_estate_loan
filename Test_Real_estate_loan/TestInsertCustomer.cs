using Real_estateloan.Model;
using System.Net;
using System.Net.Mime;
using System.Runtime.Intrinsics.X86;
using System.Text.Json;

namespace Test_Real_estate_loan
{
    public class TestInsertCustomer
    {
        private readonly HttpClient _client;

        public TestInsertCustomer(HttpClient client)
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri("https://localhost:7029"); // change the URL to match your API endpoint
        }

        [Fact]
        public async Task TestAddEmployee()
        {
            var request = new PostCustomerrequest()
            {
                CustomerSSN = "123",
                EquityAmount = 22,
                FirstName = "AKhil",
                LastName = "Gautam",
            };

            // Act
            var response = await _client.PostAsync($"/InsertCustomer/Postcustomer",
                new StringContent(request.ToJson(), System.Text.Encoding.UTF8, MediaTypeNames.Application.Json));

            // Assert
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            Assert.Equal("Employee added successfully.", content);
        }


    }
    public static class Json_convertor
    {
        
        public static string ToJson(this object obj, JsonSerializerOptions customOptions = null)
    {
        if (obj == null)
            throw new ArgumentNullException("obj", "Target object cannot be null.");
        return JsonSerializer.Serialize(obj, obj.GetType(), customOptions ?? null);
    }


}
}