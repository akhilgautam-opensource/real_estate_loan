# Real estate loan

This repository contains a C# REST API application with Get and Insert customer data functionality, 
along with Terraform code for deploying the serverless infrastructure on AWS,
and a GitLab pipeline for deploying the Terraform and C# code.
refer [Analysis and design document](/Analysis and design document.md) to get deep dive into project.

## Requirements
- .NET Core 6.0 and Visual studio(or anyother IDE)
- Terraform
- AWS Account
- git bash (or any other git tool)

## Rest API Description 
This REST api contain below listed endpoint
- GET /Adviserview: retrieves information about the customer from database
- POST /Customerview: updates information in database
- Refer API schema present inside Terraform directory([swagger.json](/Terraform/swagger.json)) 
- To run the API clone the code in local machine and open it in any IDE(Visual studio or Visual code) and 
run it over browser 

## Infrastructure as code with Terraform
This section contain the list of resources needed to run this code in side AWS
Follow below steps to run code in your machine
- Clone the repo in machine
- Open Git base/any terminal and navigate to the Terraform directory
- Run below listed command 
  - terrform init  : To initialize terraform and check previous available state of infra 
  - terrfrom plan  : To scan changes in the terraform code. 
  - terraform graph -type=plan | dot -Tsvg > graph.svg : this is optional command and this will generate graph or aws infra and dependency
  - terraform apply : To deploy infra,

## Gitlab CICD
This section is responsible for CICD orchestration for this application.
:warning: **⚠
**Running this section need good credit score**

This section contain 
- .gitlab-ci.yml file : this file contain 2 secction 1 for building C# code and another for deploying Infra and Application package.