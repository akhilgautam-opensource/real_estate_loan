# Analysis and design document

## Overview
The goal of this project is to build a simplified solution for customers to apply for a real estate loan. 
The solution should consist of a backend with a database and a REST API to handle customers' loan applications. 
The backend should store the data such as customer SSN, full name, loan amount, equity amount, and salary amount.

## Aws Infra architecture 
![](/Infra_architecture.png)

## Assumption(things needed to run this application but not covered under this project)
- Frontend web application
  - Front end application infra is present in public subnet of same aws account and expose to external user.
- AWS account is configured with basic needed settings, these things are done as the part of separate pipeline or done manually.
  - Below listed aws services are required. 
    - VPC
    - subnet
    - Network Interface
    - Route 53 : used for apigateway custom domains
    - Certificate(ACM): Need to associate with load balancer
    - System User with all needed Role and policy : Need to deploy application in aws account from CICD
- Gitlab account with aws Access token
- S3 bucket in Aws account to store Terraform state file and application deployment package

## API swagger document
![API swagger](/Terraform/swagger.json)
### Implementation and Improvement
At this moment repository is following a kind of monolithic architecture, which can be broken down into submodules to get cleaner view, avoid duplicate code and easy maintenance  
- Current repo structure and things covered
  - root
    - API application
      - DB schema and classes
      - API module
        - logging
        - api schema
        - api configuration
    - Terraform
      - config
        - dev.config
        - prod.config
      - vars
        - dev.tfvars
        - prod.tfvars
    - Gitlab
      - gitlab-ci.yml 

- Micro services structure (will update in next release)
  - Application repo
    - root 
      - Api module
        - api schema : internally calling shared lib repo to connect to database.
        - Test module
      - Terraform
        - main.tf (containing application related configuration and pass input to shared lib composition and then modules to create aws resource)
        - config
          - dev.config
          - prod.config
        - vars
          - dev.tfvars 
          - prod.tfvars
      - gitlab 
        - gitlab-ci.yml
          - importing shared lib modules for Terraform and .net builder
  
  - C# shared lib : this section will be moved to another repo and will be shared by all upcoming projects
    - DB helper 
    - logging common functions
    - General API configuration like Headers, authentication etc
  - Terraform shared lib : this section will be moved to another repo and will be shared with other project as well
    - AWS
      - Composition
        - main configuration file, and this section will point to modules
      - modules 
        - terraform code for each AWS resource like Lambda, APIgateway etc
    - Azure
      - Composition
      - modules
  - gitlab shared lib : this section will be moved to another repo and will be shared with other C# projects
    - terraform.yml : This files will be responsible for executing terraform code
    - dotnet.yml : This file will be responsible for building C# code and pushing it in S3 bucket

### Security
- Created private subnet for application layer to protect from unwanted traffic
- Implement encryption at rest and in transit for data, using services like Amazon S3, Amazon RDS.
- Used AWS Key Management Service (KMS) to manage encryption keys and protect sensitive data.
- CloudTrail can also be implemented to capture all API calls made in account, and store the logs in a secure S3 bucket. This can help us track changes made to aws resources and detect any unauthorized access
- Used strict security groups to control access to of resources, and limit traffic to only the necessary ports and protocols
- Implemented cloudwatch to monitor resources.
- Encrypted terraform statefile present in s3 bucket
- Implement authentication mechanisms to verify the identity of users before allowing them to access the API. Common authentication pratices like OAuth, and JSON Web Tokens (JWTs). Which is used for user-to-machine communication.

### Q&A
- How to perform load test
  - When it comes to load testing an API for handling a large number of requests in AWS Lambda and API Gateway, there are several things to keep in mind
    - Get a basic understanding of expected traffic based on requirement or past traffic.
    - Based on the expected traffic, we can set Lambda concurrency and autoscaling, So that our application can handel multiple request in the same time and also scale if needed.
    - Based on expected traffic we can define max and min number of parallel calls toward database.
    - During the performance we need to have better control on CloudWatch logging and monitoring because this will generate logs so many logs and can reach configured limit for some AWS services like streaming , Kinesis
    - We nedd to prepare AWS environment like setting concurrency in other lambda appplication and reserving some resources for other appliction as well So that this testing will not impact other application.
    - Define load test scenarios, cover different types of requests, different payloads, and different frequencies
    - Analyze the results and identify areas for improvement. like optimizing the code, adjusting the AWS Lambda and API Gateway configurations, or adding additional resources

- Write down your thoughts on implementations of infrastructure
  - Virtual Private Cloud: A VPC is a virtual network infrastructure that allows a logically isolated section of the AWS Cloud where we can launch AWS resources in a virtual network that we configured. Implementing a VPC enables to control network traffic and access, and we can also create subnets to divide infrastructure.
  - Load Balancer: A load balancer distributes incoming traffic across multiple lambda to ensure that the load is evenly distributed,. Load balancers are essential for ensuring high availability and fault tolerance for applications. Implementing load balancers in infrastructure enables us to distribute incoming traffic to our Lambda/servers and prevent becoming a bottleneck.
  - Auto-scaling: Auto-scaling works by monitoring infrastructure for changes in demand and automatically adding or removing resources as needed to maintain a consistent level of performance. By using auto-scaling, and can reduce costs by only paying for resources when we need them.

- Applications monitor using cloudwatch
  - Enable CloudWatch Logs for API/AWS services(this is implemented as the part of IaC)
  - Create a CloudWatch dashboard to get views of the metrics, logs, and alarms for AWS resources. we can create a dashboard specifically for monitoring API's performance and errors.
  - Configure CloudWatch Alarms(this is implemented as the part of IaC) to receive notifications when certain thresholds are exceeded.
  - Use CloudWatch Metrics to get numerical data that represent the performance of API, CloudWatch Metrics to track the latency, error rate, and number of requests made to API.