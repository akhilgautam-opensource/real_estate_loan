﻿using Org.BouncyCastle.Asn1.Crmf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBHepler
{
    public abstract class DBconfiguration
    {
        /// <summary>
        /// Username of Database
        /// </summary>
        public string UserName { get; private set; }

        /// <summary>
        /// passowrd of database
        /// </summary>
        public string Password { get; private set; }
        /// <summary>
        /// port of database
        /// </summary>
        public string Port { get; private set; }
        /// <summary>
        /// My SQL databse endpoint
        /// </summary>
        public string DBEndpoint { get; private set; }

        /// <summary>
        /// Timeout settings
        /// </summary>
        public string CommandTimeout { get; private set; }
        /// <summary>
        /// Min number of connection
        /// </summary>
        public string MinPoolSize { get; private set; }
        /// <summary>
        /// Max number of connection
        /// </summary>
        public string MaxPoolSize { get; private set; }
        /// <summary>
        /// Name of table 
        /// </summary>
        public string SchemaName { get; private set; } = "LoanBook";

        public abstract string GetWriterReaderConnectionString();
        public abstract string ReaderConnectionString();
    }
}
