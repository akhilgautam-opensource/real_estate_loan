﻿using Google.Protobuf.WellKnownTypes;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using MySql.Data.MySqlClient;
using Mysqlx.Crud;
using MySqlX.XDevAPI.Relational;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Net;
using System.Reflection;
using System.Reflection.PortableExecutable;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace DBHepler
{
    public class DBHelper : DBconfiguration
    {
        public override string GetWriterReaderConnectionString()
        {
            if (ValidateConfiguration())
                throw new ConfigurationErrorsException("DB connection string is missing some connection information");
            return $"Server={DBEndpoint};Database={SchemaName};Port={Port};SslMode=Required;Pooling=True;Username={UserName};Password={Password};Min Pool Size={MinPoolSize};Max Pool Size={MaxPoolSize};Convert Zero Datetime=True;Allow Zero Datetime=True";
        }
        private bool ValidateConfiguration()
        {
            if (String.IsNullOrWhiteSpace(UserName) || string.IsNullOrWhiteSpace(Password) || string.IsNullOrWhiteSpace(Port))
                return true;
            return false;
        }

        public override string ReaderConnectionString()
        {
            if (ValidateConfiguration())
                throw new ConfigurationErrorsException("DB connection string is missing some connection information");
            return $"Server={DBEndpoint};Database={SchemaName};Port={Port};SslMode=Required;Pooling=True;Username={UserName};Password={Password};Min Pool Size={MinPoolSize};Max Pool Size={MaxPoolSize};Convert Zero Datetime=True;Allow Zero Datetime=True";
        }



        public IEnumerable<T> getrecord<T>(DbParameter dbParameter) where T : new()
        {
            List<T> resultlist = new List<T>();
            using (SqlConnection connection = new SqlConnection(ReaderConnectionString()))
            {
                string sqlcommand = "";
                if (dbParameter == null)
                {
                     sqlcommand = $"SELECT * FROM {SchemaName}"; 
                }
                else
                {
                    sqlcommand = $"SELECT * FROM {SchemaName} WHERE {dbParameter.ParameterName} = @{dbParameter.Value} OR {dbParameter.ParameterName} = @{dbParameter.Value}";
                }
                var result =  Executesqlcommand(connection, sqlcommand);
                if (result !=null) { 
                while (result.Read())
                {
                    T obj = new();
                    dbmapper(result, obj);
                    resultlist.Add(obj);
                }
                }
                else
                {
                    throw new Exception("No record found");
                }
                connection.Close();
                result.Close();
            }

             return resultlist;

            static void dbmapper<T>(SqlDataReader result, T obj) where T : new()
            {
                for (int i = 0; i < result.FieldCount; i++)
                {
                    string fieldName = result.GetName(i);
                    PropertyInfo property = typeof(T).GetProperty(fieldName);

                    if (property != null && !result.IsDBNull(i))
                    {
                        object value = result.GetValue(i);
                        property.SetValue(obj, value);
                    }
                }
            }
        }

        public SqlDataReader Executesqlcommand(SqlConnection connection, string sqlcommand)
        {
            try
            {
                connection.Open();
                SqlCommand sqlCommand = new SqlCommand(sqlcommand, connection);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                    return reader;


            }
            catch (Exception ex)
            {

                throw;
            }
            return null;
        }
        
        public int updateRecord<T>(T customer, Dictionary<string, object> fields) 
        {
            int number_of_rows_affected;
            using (SqlConnection connection = new SqlConnection(ReaderConnectionString()))
            {

                // Build the field names and values placeholders
                string fieldNames = string.Join(",", fields.Keys);
                string fieldValues = string.Join(",", fields.Values);

                // Create the SQL query with placeholders
                string sqlcommand = $"INSERT INTO {SchemaName} ({fieldNames}) VALUES ({fieldValues})";

                using (SqlCommand commandss = new SqlCommand(sqlcommand, connection))
                {
                    connection.Open();
                    number_of_rows_affected = commandss.ExecuteNonQuery();
                    
                }
                // Close the SqlConnection
                connection.Close();
                
            }
            return number_of_rows_affected;
        }





    }
}