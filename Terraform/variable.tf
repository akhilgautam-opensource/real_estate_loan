variable "aws_region" {
  default = "eu-north-1"
  description = "AWS region"
}

variable "project_name" {
  default = "DNB_Labs"
  description = "Name of Project/family"
}
variable "Application_name" {
  default = "Realestate_loan"
}
variable "environment" {
  default = "dev"
}

variable "retention_in_days" {
  default = 180
  description = "Define how long logs will be stored in Cloudwatch"
}

variable "s3_bucket_name" {
  type    = string
  default = null
  description = "Bucket name where application package is push by gitlab"

}
variable "bucket_key" {
  type    = string
  default = null
  description = "Name of .zip artifacts."
}
# Define variables for the database user, password, and name
variable "db_user" {
  default = "admin"
  type = string
}

variable "db_password" {
  default = "password"
  type = string
}

variable "db_name" {
  default = "example_db"
  type = string
}
variable "allocatedstorage" {
  default = "10"
}
#lambda Funciton which will run the web API
variable "lambda_memory" {
  default = "120"
}

variable "lambda_concurrency" {
  description = "Min number lambda running all the time"
}