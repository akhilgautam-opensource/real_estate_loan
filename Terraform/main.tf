terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.16"
    }
  }
  #commented because trying run terraform code in my local machine and it is not having access to s3(which is assumed as the part aws account configuration)
  #moved this section into backend-config section to control values based on env.
  /*backend "s3" {
    # this bucket should be be created first then terraform will be about to recognise and push state file there.
    #Assuming that this bucket is getting created and have needed access as the part of AWS infra/account creation along with other resources like VPC,User etc
    bucket         = "realestateloan-dev-terraform-state"
    key            = "path/to/my/key"
    region         = "eu-north-1"     # here we can not use variables its a kind of Terraform limitation.
    encrypt        = true
    dynamodb_table = "realestateloan-dev-terraform-state-locks"
  }*/
}

provider "aws" {
  region = var.aws_region
  #Created a default tag which will be added in all the resources under this project.
  default_tags {
    tags = {
      Environment = var.environment
      Application = "Real_estate_credit"
    }
  }
}

locals {
  name_prefix = "${var.project_name}-${var.Application_name}-${var.environment}"
}
#aws config section
#creating aws VPC, security group,subnet here to support terraform needs but this should be created by separate pipeline and should be the part of aws account configuration
resource "aws_vpc" "vpc" {
  cidr_block           = "10.0.0.0/16" # example cidr
  enable_dns_support   = true
  enable_dns_hostnames = true
}

resource "aws_default_security_group" "vpc_security_group" {
  vpc_id = aws_vpc.vpc.id
}

resource "aws_subnet" "vpc_subnet" {
  availability_zone = data.aws_availability_zones.vpc_availability_zones.names[0]
  cidr_block        = cidrsubnet(aws_vpc.vpc.cidr_block, 8, 0)
  vpc_id            = aws_vpc.vpc.id
}
#this vpc endpoint is the entry point for private subnet of VPC
resource "aws_vpc_endpoint" "vpc_endpoint" {
  count = 3
  private_dns_enabled = false
  security_group_ids  = [aws_default_security_group.vpc_security_group.id]
  service_name        = "com.amazonaws.${data.aws_region.current.name}.execute-api"
  subnet_ids          = [aws_subnet.vpc_subnet.id]
  vpc_endpoint_type   = "Interface"
  vpc_id              = aws_vpc.vpc.id
}
#skiiping this section, This section is responsible for creating custom domain and associating it with certificate and
resource "aws_route53_hosted_zone_dnssec" "route53_private_hosted_zone" {
  hosted_zone_id = ""
}
#end of aws configuration
#IAM role which will be used as the base minimum polices to run lambda function access aws resource if needed like RDS DB, cloudwatch, APigatway
resource "aws_iam_role" "lambda_execution_role" {
  name               = "${local.name_prefix}-lambda_executation_role"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

#create policy to give access to lambda to log group
#resource "aws_iam_policy" "function_logging_policy" {
#  name = "${local.name_prefix}-loggin_policy"
#  path = "/"
#  policy = jsondecode({
#    "Version" : "2012-10-17",
#    "Statement" : [
#      {
#        "Action" : [
#          "logs:CreteLogsStream",
#          "logs:PutLogEvents"
#        ],
#        "Effect" : "Allow",
#        "Resource" : "*"
#      },
#    ]
#  })
#}
# aws policy to create and put cloudwatch log streams
resource "aws_iam_policy" "function_logging_policy" {
  name = "${local.name_prefix}-loggin_policy"
  path = "/"
  policy = data.aws_iam_policy_document.logging_policy.json
}

resource "aws_iam_role_policy_attachment" "function_logging_policy_attachment" {
  policy_arn = aws_iam_policy.function_logging_policy.arn
  role       = aws_iam_role.lambda_execution_role.name
}

#cloudwatch log group for module for lambda
resource "aws_cloudwatch_log_group" "loggroup_lambda" {
  name              = "aws/lambda/${aws_lambda_function.lambda_function.function_name}"
  retention_in_days = var.retention_in_days
  depends_on = [aws_lambda_function.lambda_function]
}

resource "aws_lambda_function" "lambda_function" {
  function_name = "${local.name_prefix}-function"
  role          = aws_iam_role.lambda_execution_role.arn
  s3_bucket     = var.s3_bucket_name
  s3_key        = var.bucket_key
  runtime       = "dotnet6"
  memory_size =  var.lambda_memory
  reserved_concurrent_executions = var.lambda_concurrency
  handler       = "DNB.Real_estateloan" # this will be depend on .net code and handler is a application entry point --_-- the typical main function
}

#APIgateway, this resource is working as a entry point for lambda and will have once needed permission to invoke lambda
resource "aws_api_gateway_rest_api" "apigateway" {
  name = local.name_prefix
  body = file("swagger.json") # this is the swagger of APIs
  endpoint_configuration {
    types            = ["PRIVATE"]
    vpc_endpoint_ids = [for vpc_endpoints in aws_vpc_endpoint.vpc_endpoint : vpc_endpoints.id]
    # this will be replaed with the VPC endpoint and assuming that it is created when AWS account is configurated
  }
  depends_on = [aws_lambda_function.lambda_function]
}

resource "aws_api_gateway_deployment" "apigateway_deployment" {
  rest_api_id = aws_api_gateway_rest_api.apigateway.id
  triggers = {
    redeployment = timestamp()  # this will cause redeployment everytime when ever pipeline is triggered, irrespective of changes in swagger
  }
}
resource "aws_api_gateway_stage" "example" {
  deployment_id = aws_api_gateway_deployment.apigateway_deployment.id
  rest_api_id   = aws_api_gateway_rest_api.apigateway.id
  stage_name    = "example"
}
#Permision needed in API gateway to invoke lambda function.
resource "aws_lambda_permission" "api-apigateway_lambda" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_function.function_name
  principal     = "apigateway.amazonaws.com"
  statement_id = "AllowExecutionFromAPIGateway"
}
/*

resource "aws_api_gateway_rest_api_policy" "apigateway_policy" {
  policy      = "" # this policy will control the incoming request toward apigateway from public subnet/other sources
  rest_api_id = aws_api_gateway_rest_api.apigateway.id
}
*/
#databse start
resource "aws_security_group" "rds_sg" {
  name = "rds_security_group"
}
resource "aws_security_group_rule" "ingress_rule" {
  from_port         = 3306
  protocol          = "tcp"
  security_group_id = aws_security_group.rds_sg.id
  to_port           = 3306
  type              = "ingress"
}
resource "aws_security_group_rule" "engress_rules" {
  from_port         = 1024
  protocol          = "tcp"
  security_group_id = aws_security_group.rds_sg.id
  to_port           = 655555
  type              = "egress"
}
#RDS database instance, creating dummy database instance to support terraform needs but this should be done as the part of separate stack
#to have more control on data, This section need more logical script to prevent unnecessary database refresh.
#the separate script should also have capability to update/add username and password in AWS parameter store and secret manager.

resource "aws_db_instance" "rds" {
  allocated_storage     = var.allocatedstorage
  storage_type = "gp2"
  engine                = "mysql"
  engine_version        = "5.7"
  instance_class        = "db.t2.micro"
  username              = "admin"
  password              = "password"
  provisioner "local-exec" {
    command = "mysql -h ${aws_db_instance.rds.address} -u ${var.db_user} -p${var.db_password} ${var.db_name} -e \"CREATE TABLE example_table (id INT PRIMARY KEY, name VARCHAR(255))\""   # this will create table
  }
  vpc_security_group_ids = [
  aws_security_group.rds_sg.id]
}

#database end

#SNS start
resource "aws_sns_topic" "lambda_failure_alert" {
  name = "${local.name_prefix}-alert"
}
#SNS subscription connect with email subscription, Endpoint can be changed to point slack group or team group
resource "aws_sns_topic_subscription" "email_subscription" {
  endpoint  = "akhil.gautam@dnb.no"
  protocol  = "email"
  topic_arn = aws_sns_topic.lambda_failure_alert.arn
}
#SNS end

#Cloudwatch subscription code to scan and filter 500 status codes in logs and send trigger email notification using SNS service.
resource "aws_cloudwatch_log_subscription_filter" "http500_subscription_filter" {
  destination_arn = aws_sns_topic.lambda_failure_alert.arn
  log_group_name  = aws_cloudwatch_log_group.loggroup_lambda.name
  filter_pattern  = "{{$.statusCode}} >= 500"
  name            = "Internal_server_error_filter"
  role_arn = aws_iam_role.lambda_execution_role.arn
  depends_on = [aws_sns_topic_subscription.email_subscription]
}