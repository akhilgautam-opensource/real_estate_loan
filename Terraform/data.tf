data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = ["sts:AssumeRole"]
    effect = "Allow"
    principals {
      identifiers = ["lambda.amazonaws.com"]
      type        = "Service"
    }
  }
}

data "aws_iam_policy_document" "logging_policy" {
  statement {
    actions = [
      "logs:CreteLogsStream",
      "logs:PutLogEvents"
    ]
    effect = "Allow"
    resources = [aws_cloudwatch_log_group.loggroup_lambda.arn]
  }
}
data "aws_region" "current" {}

data "aws_availability_zones" "vpc_availability_zones" {
  state = "available"
}
