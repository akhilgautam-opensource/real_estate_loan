﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Connections.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Real_estateloan.Model;
using System;
using System.ComponentModel.DataAnnotations;

namespace Real_estateloan.Controller
{
    [Route("[controller]")]
    [ApiController]
    /// <summary
    /// This API will update customer information in databas.
    /// </summary>
    public class InsertCustomerController : ControllerBase
    {
        private readonly ILogger<GetCustomersController> _logger;

        public InsertCustomerController(ILogger<GetCustomersController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Insert Customer info in database
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>Bool if customer information is updated in system</returns>
        /// <response code="201">Indicates successful operation.</response>
        /// <response code="400">Bad request</response>

        [HttpPost]
        public ActionResult Postcustomer(PostCustomerrequest customer)
        {
            _logger.LogTrace("Update customer info starting.");
            string? TraceID;
            if (HttpContext.Request.Headers.TryGetValue("X-TraceID", out var traceID))
            {
                TraceID = traceID;
            }

            // dictionary of field names and values
            Dictionary<string, object> fields = new Dictionary<string, object>
        {
        { "CustomerID", customer.CustomerSSN },
        { "Name", customer.FirstName },
        { "Lastname", customer.LastName },
        { "SalaryAmount", customer.SalaryAmount },
        { "EquityAmount", customer.EquityAmount },

        };
            DBHepler.DBHelper dBHelper = new DBHepler.DBHelper();
            using (SqlConnection connection = new SqlConnection(dBHelper.GetWriterReaderConnectionString()))
            {
                var rowsAffected = dBHelper.updateRecord(customer, fields);

                if (rowsAffected>0)
                {
                    return StatusCode(201);
                }
            }
            return StatusCode(500, "Failed to update data in database");
        }


    }
}

