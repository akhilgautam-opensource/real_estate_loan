﻿using Microsoft.AspNetCore.Mvc;
using Real_estateloan.Model;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using System.Data.Common;

namespace Real_estateloan.Controller
{
    /// <summary
    /// Adviser view Controller class version 1.0
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class GetCustomersController : ControllerBase
    {
        private readonly ILogger<GetCustomersController> _logger;
        readonly DbParameter dbParameter;
        public GetCustomersController(ILogger<GetCustomersController> logger)
        {
            _logger = logger;
        }
        /// <summary>
        /// Endpoint to get the list of customer
        /// </summary>
        /// <param name="headers">TraceID</param>
        /// <param name="SSN">SSN of customer</param>
        /// <param name="CustomerName">Name of Customer</param>
        /// <returns>>List of accounts.</returns>
        /// <exception cref="Exception"></exception>
        /// <response code="200">Indicates successful operation. Response body contains list of customer information</response>
        /// <response code="404">Indicates Customer not found</response>

        [HttpGet(Name = "GetCustomers"), ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<GetCustomerResponse>))]
        public ActionResult<IEnumerable<GetCustomerResponse>> GetCustomer([FromQuery] string SSN, [FromQuery] string CustomerName)
        {

            _logger.LogTrace($"calling get Customer ");
            IEnumerable<GetCustomerResponse> getCustomers;
            DBHepler.DBHelper dBHelper= new DBHepler.DBHelper();

            using (SqlConnection connection = new SqlConnection(dBHelper.ReaderConnectionString()))
            {
                try
                {
                    DBparameter(SSN, CustomerName);
                    getCustomers = dBHelper.getrecord<GetCustomerResponse>(dbParameter);
                    if (getCustomers != null && getCustomers.Count() > 0)
                        return StatusCode(200, getCustomers);
                }
                catch (Exception ex)
                {
                    _logger.LogError("UnHandeled exceptation",ex);
                    return StatusCode(500);
                }
                
            }
                return StatusCode(404);
        }

        private void DBparameter(string SSN, string CustomerName)
        {
            if (!(string.IsNullOrEmpty(CustomerName) || string.IsNullOrEmpty(SSN)))
            {
                dbParameter.ParameterName = "Name";
                dbParameter.Value = CustomerName;
                dbParameter.ParameterName = "SSN";
                dbParameter.Value = SSN;
            }
        }
    }
}
