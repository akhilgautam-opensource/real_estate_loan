﻿using System.ComponentModel.DataAnnotations;

namespace Real_estateloan.Model
{
    public class GetCustomerResponse
    {
        /// <summary>
        /// Customer identification number
        /// </summary>
        public string CustomerSSN { get; set; }
        /// <summary>
        /// First Name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Last Name
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Equity Amount
        /// </summary>
        public string EquityAmount { get; set; }

        /// <summary>
        /// SalaryAmount
        /// </summary>
        public string SalaryAmount { get; set; }
        
    }
}
