﻿using System.ComponentModel.DataAnnotations;

namespace Real_estateloan.Model
{
    public class PostCustomerrequest
    {
        /// <summary>
        /// Customer identification number
        /// </summary>
        [Required]
        public string CustomerSSN { get; set; }

        /// <summary>
        /// First name of customer
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Last name of customer
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// EquityAmount
        /// </summary>
        public decimal EquityAmount { get; set; }

        /// <summary>
        /// Monthly salary of the customer
        /// </summary>
        public decimal SalaryAmount { get; set; }

        public string ToJson()
        {
            throw new NotImplementedException();
        }
    }

}
